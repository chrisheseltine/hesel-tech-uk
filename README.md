# Setup
## Setup with VSCode (recommended)
1. clone repo to local machine, in whichever directory you wish
2. open VSCode and open the cloned folder, in the terminal type `npm install`
3. hit `F5` key to spin up a debuggable local version
4. `npm run build` & `npm run prod` to create a production build, and test it
5. for testing, install VSCode extension `Jest Test Explorer`, then run tests and view results in the `Testing` tab
6. in `Testing` tab click ellipses `...` and select `Enable autorun` to run test watcher for development
## Setup without VSCode (other IDE/Text Editor)
1. clone repo to local machine, in whichever directory you wish
2. in cmd move/cd to repo directory & `npm install`
3. `npm run start` or `npm run dev` to run in development mode, for debugging, etc.
4. `npm run build` & `npm run prod` to create a production build, and test it
5. `npm run test-watch` to run test watcher for development (test changed files only)
6. `npm run test` to run all tests and suites manually, will be done automatically before building

# Deploy
1. download Heroku CLI, install it
1. in cmd `heroku login` to gain auth to push, enter details to gain a token
2. `git push heroku master` to push and deploy

# Purpose
Landing page for Heseltech, a brand name under which I provide software engineering services. Heseltech is not yet a registered company as of 07/05/22.

# Built With
React, React Scripts, React Bootstrap, JS, HTML, CSS, Sass, Jest, React Testing Library, Serve

# Contributors
Christopher Heseltine - heseltinec@gmail.com - Owner/Principle Developer
