import React, { ReactElement } from 'react'
import {
  Container,
  Row,
  Col
} from 'react-bootstrap'

import ContentSection from 'components/ContentSection'
import contentStructure from 'content/structure.js'
import 'styles/styles.scss'

const ContentExplorer: React.FC = (): ReactElement => {
  const renderContentSections = (): ReactElement[] => {
    const contentSections = []
    for (let i = 0; i < contentStructure.length; i++) {
      contentSections.push(
        <React.Fragment key={`content-section-${i + 1}`}>
          <Row className='container-trimmed container-full'>
            <ContentSection
              index={i}
              id={contentStructure[i].id}
              headerText={contentStructure[i].headerText}
              blocks={contentStructure[i].blocks}
            />
          </Row>
          <div className='spacer-xxl' />
        </React.Fragment>
      )
    }
    return contentSections
  }

  return (
    <Container className='container-content-explorer container-trimmed container-full'>
      <Row className='container-trimmed container-full'>
        <Col className='container-trimmed container-full'>
          <Container className='container-trimmed container-full'>
            <div className='spacer-xxl' />
            {renderContentSections()}
          </Container>
        </Col>
      </Row>
    </Container>
  )
}

export default ContentExplorer
