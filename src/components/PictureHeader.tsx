import React, { ReactElement } from 'react'

import Picture from 'components/Picture'
import 'styles/styles.scss'

const PictureHeader: React.FC<Record<string, never>> = (): ReactElement => {
  return (
    <Picture
      src={`${String(process.env.PUBLIC_URL)}/img/header.jpg`}
      alt='Cambodian Sunset'
      className='picture-header'
    />
  )
}

export default PictureHeader
