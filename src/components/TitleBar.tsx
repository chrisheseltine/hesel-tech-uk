import React, { ReactElement } from 'react'
import {
  Container,
  Row,
  Col
} from 'react-bootstrap'

import Icon from 'components/Icon'
import 'styles/styles.scss'

const TitleBar: React.FC<Record<string, never>> = (): ReactElement => {
  return (
    <Container className='container-titlebar container-trimmed container-full'>
      <Row className='container-trimmed container-full'>
        <Col className='container-trimmed' />
        <Col
          xs='4'
          className='container-titlebar-header container-trimmed'
        >
          <h1 className='text-titlebar-header'>
            Christopher Heseltine
          </h1>
          <h2 className='text-titlebar-sub-header'>
            Freelance Software Engineer
          </h2>
        </Col>
        <Col className='container-trimmed' />
        <Col
          xs='2'
          className='container-trimmed'
        >
          <div className='container-icons'>
            {
              [
                {
                  key: 'email',
                  href: 'mailto:heseltinec@gmail.com',
                  src: `${String(process.env.PUBLIC_URL)}/img/icon-email.png`,
                  alt: 'Email'
                },
                {
                  key: 'facebook',
                  href: 'https://www.facebook.com/chris.heseltine',
                  src: `${String(process.env.PUBLIC_URL)}/img/icon-facebook.png`,
                  alt: 'Facebook'
                },
                {
                  key: 'stackoverflow',
                  href: 'https://stackoverflow.com/users/5640115/chrisheseltine',
                  src: `${String(process.env.PUBLIC_URL)}/img/icon-stackoverflow.png`,
                  alt: 'Stack Overflow'
                },
                {
                  key: 'linkedin',
                  href: 'https://www.linkedin.com/in/chrisheseltine',
                  src: `${String(process.env.PUBLIC_URL)}/img/icon-linkedin.png`,
                  alt: 'LinkedIn'
                },
                {
                  key: 'github',
                  href: 'https://github.com/chrisheseltine',
                  src: `${String(process.env.PUBLIC_URL)}/img/icon-github.png`,
                  alt: 'GitHub'
                },
                {
                  key: 'gitlab',
                  href: 'https://gitlab.com/chrisheseltine',
                  src: `${String(process.env.PUBLIC_URL)}/img/icon-gitlab.webp`,
                  alt: 'GitLab'
                }
              ].map(o => {
                return (
                  <Icon
                    key={o.key}
                    href={o.href}
                    src={o.src}
                    alt={o.alt}
                  />
                )
              })
            }
          </div>
        </Col>
        <Col className='container-trimmed' />
      </Row>
    </Container>
  )
}

export default TitleBar
