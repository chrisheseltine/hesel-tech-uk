import React, { ReactElement } from 'react'

import Picture from 'components/Picture'
import { isFalsy } from 'helpers'
import 'styles/styles.scss'

interface Props {
  src: string
  alt: string
}

const Thumbnail: React.FC<Props> = (props: Props): ReactElement => {
  if (isFalsy(props.src)) { throw new ReferenceError("Attempted to invoke 'Thumbnail' component without an 'src' prop!") }
  if (typeof props.src !== 'string') { throw new TypeError("Attempted to invoke 'Thumbnail' component with an invalid 'src' prop!") }

  if (isFalsy(props.alt)) { throw new ReferenceError("Attempted to invoke 'Thumbnail' component without an 'alt' prop!") }
  if (typeof props.alt !== 'string') { throw new TypeError("Attempted to invoke 'Thumbnail' component with an invalid 'alt' prop!") }

  return (
    <a
      href={props.src}
      target='blank'
      rel='noopener noreferer'
      className='link-thumbail'
      data-testid='link-thumbnail'
    >
      <Picture
        src={props.src}
        alt={props.alt}
        className='picture-thumbnail'
        dataTestId='picture-thumbnail'
      />
    </a>
  )
}

export default Thumbnail
