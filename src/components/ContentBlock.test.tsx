import React from 'react'
import { render } from '@testing-library/react'

import ContentBlock from 'components/ContentBlock'
const constants = require('constants.js')
const { CONTENTBLOCK_TYPE_CIRCLE } = constants

const TEST_CONTENTBLOCKTYPE = CONTENTBLOCK_TYPE_CIRCLE
const TEST_CONTENT = 'Hello World!'
const TEST_BLOCK = {
  type: TEST_CONTENTBLOCKTYPE,
  content: TEST_CONTENT
}

it('renders', () => {
  render(<ContentBlock block={TEST_BLOCK} />)
})

describe('renders correctly', () => {
  beforeEach(() => {
    // Hides caught errors in expect() that are needlessly printed to the jest-dom console
    jest.spyOn(console, 'error').mockImplementation(() => { })
  })

  it('throws an error when initialised with no block prop', () => {
    expect(() => {
      render(<ContentBlock />)
    }).toThrow(ReferenceError)
  })

  it('throws an error when initialised with an invalid block prop as number', () => {
    expect(() => {
      render(<ContentBlock block={6} />)
    }).toThrow(TypeError)
  })

  it('throws an error when initialised with an invalid block prop as function', () => {
    expect(() => {
      render(<ContentBlock block={() => { console.log() }} />)
    }).toThrow(TypeError)
  })

  it('throws an error when initialised with an invalid block prop as undefined', () => {
    expect(() => {
      render(<ContentBlock block={undefined} />)
    }).toThrow(ReferenceError)
  })

  it('throws an error when initialised with an invalid block prop as boolean', () => {
    expect(() => {
      render(<ContentBlock block />)
    }).toThrow(TypeError)
  })

  it('throws an error when initialised with an invalid block prop as string', () => {
    expect(() => {
      render(<ContentBlock block='test string' />)
    }).toThrow(TypeError)
  })

  it('sets the correct class', () => {
    const { getByTestId } = render(
      <ContentBlock block={TEST_BLOCK} />
    )

    const CONTAINER_TEST_ID = 'container-content-block'
    const container = getByTestId(CONTAINER_TEST_ID)
    expect(container).toHaveAttribute('class', expect.stringContaining('container-content-block-circle'))
  })

  it('sets the correct text content', () => {
    const { getByTestId } = render(
      <ContentBlock block={TEST_BLOCK} />
    )

    const TEXT_TEST_ID = 'text-content-block'
    const text = getByTestId(TEXT_TEST_ID)
    expect(text).toHaveTextContent(TEST_CONTENT)
  })
})

// Put yourself in the mind of the user when writing tests, and only tests things that interest them

/* Other tests could be:
1. it renders correctly, for components that have rendered output influenced by props
2. it handles state correctly, for components with state and conditionals
3. it handles events correctly, for components with interaction and events
4. it handles edge cases, for components using arrays or user data such as age
*/
