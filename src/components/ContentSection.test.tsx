import React from 'react'
import { render } from '@testing-library/react'

import ContentSection from 'components/ContentSection'
import { CONTENTBLOCK_TYPE_CIRCLE } from 'constants.js'

const TEST_INDEX = 0
const TEST_ID = 'skills'
const TEST_HEADERTEXT = 'Skills'
const TEST_CONTENTBLOCKTYPE = CONTENTBLOCK_TYPE_CIRCLE
const TEST_BLOCKS = [
  {
    type: TEST_CONTENTBLOCKTYPE,
    content: 'I am so skilled...'
  },
  {
    type: TEST_CONTENTBLOCKTYPE,
    content: 'At what it is I do.'
  }
]

it('renders', () => {
  render(
    <ContentSection
      index={TEST_INDEX}
      id={TEST_ID}
      headerText={TEST_HEADERTEXT}
      blocks={TEST_BLOCKS}
    />
  )
})

describe('renders correctly', () => {
  beforeEach(() => {
    // Hides caught errors in expect() that are needlessly printed to the jest-dom console
    jest.spyOn(console, 'error').mockImplementation(() => { })
  })

  it('throws an error when initialised with no index prop', () => {
    expect(() => {
      render(
        <ContentSection
          id={TEST_ID}
          headerText={TEST_HEADERTEXT}
          blocks={TEST_BLOCKS}
        />
      )
    }).toThrow(ReferenceError)
  })

  it('throws an error when initialised with an invalid index prop as function', () => {
    expect(() => {
      render(
        <ContentSection
          index={() => { console.log() }}
          id={TEST_ID}
          headerText={TEST_HEADERTEXT}
          blocks={TEST_BLOCKS}
        />
      )
    }).toThrow(TypeError)
  })

  it('throws an error when initialised with an invalid index prop as undefined', () => {
    expect(() => {
      render(
        <ContentSection
          index={undefined}
          id={TEST_ID}
          headerText={TEST_HEADERTEXT}
          blocks={TEST_BLOCKS}
        />
      )
    }).toThrow(ReferenceError)
  })

  it('throws an error when initialised with an invalid index prop as boolean', () => {
    expect(() => {
      render(
        <ContentSection
          index
          id={TEST_ID}
          headerText={TEST_HEADERTEXT}
          blocks={TEST_BLOCKS}
        />
      )
    }).toThrow(TypeError)
  })

  it('throws an error when initialised with no id prop', () => {
    expect(() => {
      render(
        <ContentSection
          index={TEST_INDEX}
          headerText={TEST_HEADERTEXT}
          blocks={TEST_BLOCKS}
        />
      )
    }).toThrow(ReferenceError)
  })

  it('throws an error when initialised with an invalid id prop as number', () => {
    expect(() => {
      render(
        <ContentSection
          index={TEST_INDEX}
          id={6}
          headerText={TEST_HEADERTEXT}
          blocks={TEST_BLOCKS}
        />
      )
    }).toThrow(TypeError)
  })

  it('throws an error when initialised with an invalid id prop as function', () => {
    expect(() => {
      render(
        <ContentSection
          index={TEST_INDEX}
          id={() => { console.log() }}
          headerText={TEST_HEADERTEXT}
          blocks={TEST_BLOCKS}
        />
      )
    }).toThrow(TypeError)
  })

  it('throws an error when initialised with an invalid id prop as undefined', () => {
    expect(() => {
      render(
        <ContentSection
          index={TEST_INDEX}
          id={undefined}
          headerText={TEST_HEADERTEXT}
          blocks={TEST_BLOCKS}
        />
      )
    }).toThrow(ReferenceError)
  })

  it('throws an error when initialised with an invalid id prop as boolean', () => {
    expect(() => {
      render(
        <ContentSection
          index={TEST_INDEX}
          id
          headerText={TEST_HEADERTEXT}
          blocks={TEST_BLOCKS}
        />
      )
    }).toThrow(TypeError)
  })

  it('throws an error when initialised with no headerText prop', () => {
    expect(() => {
      render(
        <ContentSection
          index={TEST_INDEX}
          id={TEST_ID}
          blocks={TEST_BLOCKS}
        />
      )
    }).toThrow(ReferenceError)
  })

  it('throws an error when initialised with an invalid headerText prop as number', () => {
    expect(() => {
      render(
        <ContentSection
          index={TEST_INDEX}
          id={TEST_ID}
          headerText={6}
          blocks={TEST_BLOCKS}
        />
      )
    }).toThrow(TypeError)
  })

  it('throws an error when initialised with an invalid headerText prop as function', () => {
    expect(() => {
      render(
        <ContentSection
          index={TEST_INDEX}
          id={TEST_ID}
          headerText={() => { console.log() }}
          blocks={TEST_BLOCKS}
        />
      )
    }).toThrow(TypeError)
  })

  it('throws an error when initialised with an invalid headerText prop as undefined', () => {
    expect(() => {
      render(
        <ContentSection
          index={TEST_INDEX}
          id={TEST_ID}
          headerText={undefined}
          blocks={TEST_BLOCKS}
        />
      )
    }).toThrow(ReferenceError)
  })

  it('throws an error when initialised with an invalid headerText prop as boolean', () => {
    expect(() => {
      render(
        <ContentSection
          index={TEST_INDEX}
          id={TEST_ID}
          headerText
          blocks={TEST_BLOCKS}
        />
      )
    }).toThrow(TypeError)
  })

  it('throws an error when initialised with no blocks prop', () => {
    expect(() => {
      render(
        <ContentSection
          index={TEST_INDEX}
          id={TEST_ID}
          headerText={TEST_HEADERTEXT}
        />
      )
    }).toThrow(ReferenceError)
  })

  it('throws an error when initialised with an invalid blocks prop as number', () => {
    expect(() => {
      render(
        <ContentSection
          index={TEST_INDEX}
          id={TEST_ID}
          headerText={TEST_HEADERTEXT}
          blocks={6}
        />
      )
    }).toThrow(TypeError)
  })

  it('throws an error when initialised with an invalid blocks prop as string', () => {
    expect(() => {
      render(
        <ContentSection
          index={TEST_INDEX}
          id={TEST_ID}
          headerText={TEST_HEADERTEXT}
          blocks='test string'
        />
      )
    }).toThrow(TypeError)
  })

  it('throws an error when initialised with an invalid blocks prop as undefined', () => {
    expect(() => {
      render(
        <ContentSection
          index={TEST_INDEX}
          id={TEST_ID}
          headerText={TEST_HEADERTEXT}
          blocks={undefined}
        />
      )
    }).toThrow(ReferenceError)
  })

  it('throws an error when initialised with an invalid blocks prop as boolean', () => {
    expect(() => {
      render(
        <ContentSection
          index={TEST_INDEX}
          id={TEST_ID}
          headerText={TEST_HEADERTEXT}
          blocks
        />
      )
    }).toThrow(TypeError)
  })

  it('throws an error when initialised with an invalid blocks prop as empty array', () => {
    expect(() => {
      render(
        <ContentSection
          index={TEST_INDEX}
          id={TEST_ID}
          headerText={TEST_HEADERTEXT}
          blocks={[]}
        />
      )
    }).toThrow(ReferenceError)
  })

  it('sets the correct header text', () => {
    const { getByTestId } = render(
      <ContentSection
        index={TEST_INDEX}
        id={TEST_ID}
        headerText={TEST_HEADERTEXT}
        blocks={TEST_BLOCKS}
      />
    )

    const TEXT_TEST_ID = 'text-content-section-header'
    const text = getByTestId(TEXT_TEST_ID)
    expect(text).toHaveTextContent(TEST_HEADERTEXT)
  })
})

// Put yourself in the mind of the user when writing tests, and only tests things that interest them

/* Other tests could be:
1. it renders correctly, for components that have rendered output influenced by props
2. it handles state correctly, for components with state and conditionals
3. it handles events correctly, for components with interaction and events
4. it handles edge cases, for components using arrays or user data such as age
*/
