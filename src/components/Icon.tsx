import React, { ReactElement } from 'react'

import Picture from 'components/Picture'
import { isFalsy } from 'helpers'
import 'styles/styles.scss'

interface Props {
  href: string
  src: string
  alt: string
}

const Icon: React.FC<Props> = (props: Props): ReactElement => {
  if (isFalsy(props.href)) { throw new ReferenceError("Attempted to invoke 'Icon' component without an 'href' prop!") }
  if (typeof props.href !== 'string') { throw new TypeError("Attempted to invoke 'Icon' component with an invalid 'href' prop!") }

  if (isFalsy(props.src)) { throw new ReferenceError("Attempted to invoke 'Icon' component without an 'src' prop!") }
  if (typeof props.src !== 'string') { throw new TypeError("Attempted to invoke 'Icon' component with an invalid 'src' prop!") }

  if (isFalsy(props.alt)) { throw new ReferenceError("Attempted to invoke 'Icon' component without an 'alt' prop!") }
  if (typeof props.alt !== 'string') { throw new TypeError("Attempted to invoke 'Icon' component with an invalid 'alt' prop!") }

  return (
    <a
      href={props.href}
      target='blank'
      rel='noopener noreferer'
      className='link-icon'
      data-testid='link-icon'
    >
      <Picture
        src={props.src}
        alt={props.alt}
        className='picture-icon'
        dataTestId='picture-icon'
      />
    </a>
  )
}

export default Icon
