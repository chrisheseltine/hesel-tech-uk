import React, { ReactElement } from 'react'

import Picture from 'components/Picture'
import 'styles/styles.scss'
import packageJSON from '../../package.json'

const Footer: React.FC<Record<string, never>> = (): ReactElement => {
  return (
    <>
      <div className='container-footer-top'>
        <Picture
          src={`${String(process.env.PUBLIC_URL)}/img/necro.gif`}
          alt='Necromancer'
          className='picture-necro'
        />
      </div>
      <div className='container-footer-bottom'>
        <p className='text-version'>Version: {packageJSON.version}</p>
      </div>
      <div className='container-bottom-border' />
    </>
  )
}

export default Footer
