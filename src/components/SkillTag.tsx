import React, { ReactElement } from 'react'

import {
  SKILLTAG_TYPE_FRONTEND,
  SKILLTAG_TYPE_BACKEND,
  SKILLTAG_TYPE_DEVOPS,
  SKILLTAG_TYPE_GAMES,
  SKILLTAG_TYPE_META
} from 'constants.js'
import { isFalsy } from 'helpers'
import 'styles/styles.scss'

interface Props {
  type: string
  text: string
}

const SkillTag: React.FC<Props> = (props: Props): ReactElement => {
  if (isFalsy(props.type)) { throw new ReferenceError("Attempted to invoke 'SkillTag' component without a 'type' prop!") }
  if (typeof props.type !== 'string') { throw new TypeError("Attempted to invoke 'SkillTag' component with an invalid 'type' prop!") }

  if (isFalsy(props.text)) { throw new ReferenceError("Attempted to invoke 'SkillTag' component without a 'text' prop!") }
  if (typeof props.text !== 'string') { throw new TypeError("Attempted to invoke 'SkillTag' component with an invalid 'text' prop!") }

  const getClassFromType = (type: string): string => {
    switch (type) {
      case SKILLTAG_TYPE_FRONTEND:
        return 'text-skill-tag-red'
      case SKILLTAG_TYPE_BACKEND:
        return 'text-skill-tag-blue'
      case SKILLTAG_TYPE_DEVOPS:
        return 'text-skill-tag-green'
      case SKILLTAG_TYPE_GAMES:
        return 'text-skill-tag-pink'
      case SKILLTAG_TYPE_META:
        return 'text-skill-tag-orange'
      default:
        throw new Error("Attempted to invoke 'SkillTag' component with an invalid 'type' prop! The type must be one of the pre-defined types.")
    }
  }

  return (
    <span
      className={`text-skill-tag ${getClassFromType(props.type)}`}
      data-testid='text-skill-tag'
    >
      {props.text}
    </span>
  )
}

export default SkillTag
