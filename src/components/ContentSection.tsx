import React, { ReactElement } from 'react'
import {
  Container,
  Row,
  Col
} from 'react-bootstrap'

import ContentBlock from 'components/ContentBlock'
import IContentBlock from 'interfaces/ContentBlock.interface'
import { isFalsy } from 'helpers'
import 'styles/styles.scss'

interface Props {
  index: number
  id: string
  headerText: string
  blocks: IContentBlock[]
}

const ContentSection: React.FC<Props> = (props: Props): ReactElement => {
  if (isFalsy(props.index) && props.index !== 0) { throw new ReferenceError("Attempted to invoke 'ContentSection' component without an 'index' prop!") }
  if (typeof props.index !== 'number') {
    let attempedIndexParse
    try {
      attempedIndexParse = parseInt(props.index)
    } catch (err) {
      throw new TypeError("Attempted to invoke 'ContentSection' component with an invalid 'index' prop! Must be of type 'number' or parsable to an integer.")
    }
    if (isFalsy(attempedIndexParse)) { throw new TypeError("Attempted to invoke 'ContentSection' component with an invalid 'index' prop! Must be of type 'number' or parsable to an integer.") }
  }

  if (isFalsy(props.id)) { throw new ReferenceError("Attempted to invoke 'ContentSection' component without an 'id' prop!") }
  if (typeof props.id !== 'string') { throw new TypeError("Attempted to invoke 'ContentSection' component with an invalid 'id' prop!") }

  if (isFalsy(props.headerText)) { throw new ReferenceError("Attempted to invoke 'ContentSection' component without a 'headerText' prop!") }
  if (typeof props.headerText !== 'string') { throw new TypeError("Attempted to invoke 'ContentSection' component with an invalid 'headerText' prop!") }

  if (isFalsy(props.blocks)) { throw new ReferenceError("Attempted to invoke 'ContentSection' component without a 'blocks' prop!") }
  if (!Array.isArray(props.blocks)) { throw new TypeError("Attempted to invoke 'ContentSection' component with an invalid 'blocks' prop! Must be of type 'array'.") }
  if (props.blocks.length < 1) { throw new ReferenceError("Attempted to invoke 'ContentSection' component with an empty 'blocks' prop! Array must have length of at least 1.") }

  return (
    <Container
      id={props.id}
      className='container-content-section container-trimmed container-full'
      data-testid='container-content-section'
    >
      <div className='spacer-md' />
      <Row className='container-trimmed container-full'>
        <Col className='container-trimmed' />
        <Col
          xs='auto'
          className='container-trimmed'
        >
          <h3
            className={props.index % 2 === 0 ? 'text-content-section-header-even' : 'text-content-section-header-odd'}
            data-testid='text-content-section-header'
          >
            {props.headerText}
          </h3>
        </Col>
        <Col className='container-trimmed' />
      </Row>
      <Row className='container-trimmed container-full d-flex justify-content-center'>
        {props.blocks.map((b: IContentBlock, j) => {
          return (
            <ContentBlock
              key={`block-${j}`}
              block={b}
            />
          )
        })}
      </Row>
    </Container>
  )
}

export default ContentSection
