import React from 'react'
import { render } from '@testing-library/react'

import NavBar from 'components/NavBar'

it('renders', () => {
  render(<NavBar />)
})

// Put yourself in the mind of the user when writing tests, and only tests things that interest them

/* Other tests could be:
1. it renders correctly, for components that have rendered output influenced by props
2. it handles state correctly, for components with state and conditionals
3. it handles events correctly, for components with interaction and events
4. it handles edge cases, for components using arrays or user data such as age
*/
