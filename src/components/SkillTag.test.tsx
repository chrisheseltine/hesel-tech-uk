import React from 'react'
import { render } from '@testing-library/react'

import SkillTag from 'components/SkillTag'
import { SKILLTAG_TYPE_FRONTEND } from 'constants.js'

const TEST_TYPE = SKILLTAG_TYPE_FRONTEND
const TEST_TEXT = 'HTML'

it('renders', () => {
  render(
    <SkillTag
      type={TEST_TYPE}
      text={TEST_TEXT}
    />
  )
})

describe('renders correctly', () => {
  beforeEach(() => {
    // Hides caught errors in expect() that are needlessly printed to the jest-dom console
    jest.spyOn(console, 'error').mockImplementation(() => { })
  })

  it('throws an error when initialised with no type prop', () => {
    expect(() => {
      render(
        <SkillTag text={TEST_TEXT} />
      )
    }).toThrow(ReferenceError)
  })

  it('throws an error when initialised with an invalid type prop as function', () => {
    expect(() => {
      render(
        <SkillTag
          type={() => { console.log() }}
          text={TEST_TEXT}
        />
      )
    }).toThrow(TypeError)
  })

  it('throws an error when initialised with an invalid type prop as undefined', () => {
    expect(() => {
      render(
        <SkillTag
          type={undefined}
          text={TEST_TEXT}
        />
      )
    }).toThrow(ReferenceError)
  })

  it('throws an error when initialised with an invalid type prop as boolean', () => {
    expect(() => {
      render(
        <SkillTag
          type
          text={TEST_TEXT}
        />
      )
    }).toThrow(TypeError)
  })

  it('throws an error when initialised with no text prop', () => {
    expect(() => {
      render(
        <SkillTag type={TEST_TYPE} />
      )
    }).toThrow(ReferenceError)
  })

  it('throws an error when initialised with an invalid text prop as number', () => {
    expect(() => {
      render(
        <SkillTag
          type={TEST_TYPE}
          text={6}
        />
      )
    }).toThrow(TypeError)
  })

  it('throws an error when initialised with an invalid text prop as function', () => {
    expect(() => {
      render(
        <SkillTag
          type={TEST_TYPE}
          text={() => { console.log() }}
        />
      )
    }).toThrow(TypeError)
  })

  it('throws an error when initialised with an invalid text prop as undefined', () => {
    expect(() => {
      render(
        <SkillTag
          type={TEST_TYPE}
          text={undefined}
        />
      )
    }).toThrow(ReferenceError)
  })

  it('throws an error when initialised with an invalid text prop as boolean', () => {
    expect(() => {
      render(
        <SkillTag
          type={TEST_TYPE}
          text
        />
      )
    }).toThrow(TypeError)
  })

  it('sets the correct attributes', () => {
    const { getByTestId } = render(
      <SkillTag
        type={TEST_TYPE}
        text={TEST_TEXT}
      />
    )

    const TEXT_TEST_ID = 'text-skill-tag'
    const text = getByTestId(TEXT_TEST_ID)
    expect(text).toHaveTextContent(TEST_TEXT)
  })
})

// Put yourself in the mind of the user when writing tests, and only tests things that interest them

/* Other tests could be:
1. it renders correctly, for components that have rendered output influenced by props
2. it handles state correctly, for components with state and conditionals
3. it handles events correctly, for components with interaction and events
4. it handles edge cases, for components using arrays or user data such as age
*/
