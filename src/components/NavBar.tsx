import React, { ReactElement } from 'react'
import {
  Container,
  Row,
  Col,
  Nav
} from 'react-bootstrap'

import 'styles/styles.scss'
import loadedInfo from 'content/info.json'

const NavBar: React.FC<Record<string, never>> = (): ReactElement => {
  return (
    <Container className='container-nav container-trimmed container-full'>
      <Row className='container-trimmed container-full'>
        <Col className='container-trimmed' />
        <Col
          xs='auto'
          className='container-trimmed'
        >
          <Nav>
            {[
              {
                label: 'About Me',
                labelSmall: 'Abt',
                href: '#about-me'
              },
              {
                label: 'Testimonials',
                labelSmall: 'Tst',
                href: '#testimonials'
              },
              {
                label: 'Skills',
                labelSmall: 'Skl',
                href: '#skills'
              },
              {
                label: 'Experience',
                labelSmall: 'Exp',
                href: '#experience'
              },
              {
                label: 'Code Samples',
                labelSmall: 'Cod',
                href: '#code-samples'
              },
              {
                label: 'Background',
                labelSmall: 'Bkg',
                href: '#background'
              },
              {
                label: 'Download CV',
                labelSmall: 'CV',
                href: loadedInfo.resumeUrl
              }
            ].map(o => {
              return (
                <Nav.Item key={`nav-item-${o.label}`}>
                  <Nav.Link href={o.href}>
                    <span className='text-nav-item-lg'>{o.label}</span>
                    <span className='text-nav-item-sm'>{o.labelSmall}</span>
                  </Nav.Link>
                </Nav.Item>
              )
            })}
          </Nav>
        </Col>
        <Col className='container-trimmed' />
      </Row>
    </Container>
  )
}

export default NavBar
