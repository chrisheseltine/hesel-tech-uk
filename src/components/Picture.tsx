import React, { ReactElement } from 'react'
import {
  Image
} from 'react-bootstrap'

import { isFalsy } from 'helpers'
import 'styles/styles.scss'

interface Props {
  src: string
  alt: string
  className: string
  dataTestId?: string
}

const Picture: React.FC<Props> = (props: Props): ReactElement => {
  if (isFalsy(props.src)) { throw new ReferenceError("Attempted to invoke 'Picture' component without a 'src' prop!") }
  if (typeof props.src !== 'string') { throw new TypeError("Attempted to invoke 'Picture' component with an invalid 'src' prop!") }

  if (isFalsy(props.alt)) { throw new ReferenceError("Attempted to invoke 'Picture' component without an 'alt' prop!") }
  if (typeof props.alt !== 'string') { throw new TypeError("Attempted to invoke 'Picture' component with an invalid 'alt' prop!") }

  if (!isFalsy(props.className) && typeof props.className !== 'string') { throw new TypeError("Attempted to invoke 'Picture' component with an invalid 'className' prop!") }

  if (!isFalsy(props.dataTestId) && typeof props.dataTestId !== 'string') { throw new TypeError("Attempted to invoke 'Picture' component with an invalid 'dataTestId' prop!") }

  return (
    <Image
      src={props.src}
      alt={props.alt}
      className={isFalsy(props.className) ? 'picture' : props.className}
      data-testid={isFalsy(props.dataTestId) ? 'picture' : props.dataTestId}
    />
  )
}

export default Picture
