import React, { ReactElement } from 'react'
import {
  Container,
  Row,
  Col
} from 'react-bootstrap'

import {
  CONTENTBLOCK_TYPE_CIRCLE,
  CONTENTBLOCK_TYPE_SQUARE
} from 'constants.js'
import { isFalsy } from '../helpers'
import IContentBlock from 'interfaces/ContentBlock.interface'
import 'styles/styles.scss'

interface Props {
  block: IContentBlock
}

const ContentBlock: React.FC<Props> = (props: Props): ReactElement => {
  if (isFalsy(props.block)) { throw new ReferenceError("Attempted to invoke 'ContentBlock' component without a 'block' prop!") }
  if (typeof props.block !== 'object') { throw new TypeError("Attempted to invoke 'ContentBlock' component with an invalid 'block' prop! Must be of type 'object'.") }

  if (props.block.type === null || props.block.type === undefined) { throw new ReferenceError("Attempted to invoke 'ContentBlock' component with an invalid 'block' prop! The block must have a type set.") }
  if (typeof props.block.type !== 'string') { throw new TypeError("Attempted to invoke 'ContentBlock' component with an invalid 'block' prop! The block type must be defined as a string.") }
  if (props.block.type !== CONTENTBLOCK_TYPE_CIRCLE && props.block.type !== CONTENTBLOCK_TYPE_SQUARE) { throw new Error("Attempted to invoke 'ContentBlock' component with an invalid 'block' prop! The block type must be one of the pre-defined types.") }

  if (isFalsy(props.block.content)) { throw new ReferenceError("Attempted to invoke 'ContentBlock' component with an invalid 'block' prop! Each block must have content set.") }
  if (typeof props.block.content !== 'string' && typeof props.block.content !== 'object') { throw new TypeError("Attempted to invoke 'ContentBlock' component with an invalid 'block' prop! Content must be either a string or JSX object.") }

  const getMainClassFromType = (type: string): string => {
    if (type === CONTENTBLOCK_TYPE_CIRCLE) return 'container-content-block-circle'
    else return 'container-content-block-square'
  }

  const getBorderClassFromType = (type: string): string => {
    if (type === CONTENTBLOCK_TYPE_CIRCLE) return 'container-content-block-border-circle'
    else return 'container-content-block-border-square'
  }

  return (
    <Container
      className={`${getMainClassFromType(props.block.type)} container-content-block container-trimmed container-full`}
      data-testid='container-content-block'
    >
      <Container className={`${getBorderClassFromType(props.block.type)} container-content-block-border container-trimmed container-full`}>
        <Row className='container-trimmed container-full'>
          <Col className='container-trimmed' />
          <Col
            xs='auto'
            className='container-trimmed'
          >
            <span data-testid='text-content-block'>
              {props.block.content}
            </span>
          </Col>
          <Col className='container-trimmed' />
        </Row>
      </Container>
    </Container>
  )
}

export default ContentBlock
