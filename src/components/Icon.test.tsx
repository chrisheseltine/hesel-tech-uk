import React from 'react'
import { render } from '@testing-library/react'

import Icon from 'components/Icon'

const TEST_SRC = 'data:image/gif;base64,R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw=='
const TEST_ALT = 'Red Cartoon Car'
const TEST_HREF = 'http://example.com/'

it('renders', () => {
  render(
    <Icon
      href={TEST_HREF}
      src={TEST_SRC}
      alt={TEST_ALT}
    />
  )
})

describe('renders correctly', () => {
  beforeEach(() => {
    // Hides caught errors in expect() that are needlessly printed to the jest-dom console
    jest.spyOn(console, 'error').mockImplementation(() => {})
  })

  it('throws an error when initialised with no href prop', () => {
    expect(() => {
      render(
        <Icon
          src={TEST_SRC}
          alt={TEST_ALT}
        />
      )
    }).toThrow(ReferenceError)
  })

  it('throws an error when initialised with an invalid href prop', () => {
    expect(() => {
      render(
        <Icon
          href={6}
          src={TEST_SRC}
          alt={TEST_ALT}
        />
      )
    }).toThrow(TypeError)
    expect(() => {
      render(
        <Icon
          href={() => { console.log() }}
          src={TEST_SRC}
          alt={TEST_ALT}
        />
      )
    }).toThrow(TypeError)
    expect(() => {
      render(
        <Icon
          href={undefined}
          src={TEST_SRC}
          alt={TEST_ALT}
        />
      )
    }).toThrow(ReferenceError)
    expect(() => {
      render(
        <Icon
          href
          src={TEST_SRC}
          alt={TEST_ALT}
        />
      )
    }).toThrow(TypeError)
  })

  it('throws an error when initialised with no src prop', () => {
    expect(() => {
      render(
        <Icon
          href={TEST_HREF}
          alt={TEST_ALT}
        />
      )
    }).toThrow(ReferenceError)
  })

  it('throws an error when initialised with an invalid src prop', () => {
    expect(() => {
      render(
        <Icon
          href={TEST_HREF}
          src={6}
          alt={TEST_ALT}
        />
      )
    }).toThrow(TypeError)
    expect(() => {
      render(
        <Icon
          href={TEST_HREF}
          src={() => { console.log() }}
          alt={TEST_ALT}
        />
      )
    }).toThrow(TypeError)
    expect(() => {
      render(
        <Icon
          href={TEST_HREF}
          src={undefined}
          alt={TEST_ALT}
        />
      )
    }).toThrow(ReferenceError)
    expect(() => {
      render(
        <Icon
          href={TEST_HREF}
          src
          alt={TEST_ALT}
        />
      )
    }).toThrow(TypeError)
    expect(() => {
      render(
        <Icon
          href={TEST_HREF}
          src={[]}
          alt={TEST_ALT}
        />
      )
    }).toThrow(TypeError)
  })

  it('throws an error when initialised with no alt prop', () => {
    expect(() => {
      render(
        <Icon
          href={TEST_HREF}
          src={TEST_SRC}
        />
      )
    }).toThrow(ReferenceError)
  })

  it('throws an error when initialised with an invalid alt prop', () => {
    expect(() => {
      render(
        <Icon
          href={TEST_HREF}
          src={TEST_SRC}
          alt={6}
        />
      )
    }).toThrow(TypeError)
    expect(() => {
      render(
        <Icon
          href={TEST_HREF}
          src={TEST_SRC}
          alt={() => { console.log() }}
        />
      )
    }).toThrow(TypeError)
  })

  it('sets the correct attributes', () => {
    const { getByTestId } = render(
      <Icon
        href={TEST_HREF}
        src={TEST_SRC}
        alt={TEST_ALT}
      />
    )

    const LINK_TEST_ID = 'link-icon'
    const link = getByTestId(LINK_TEST_ID)
    expect(link).toHaveAttribute('href', expect.stringContaining(TEST_HREF))

    const PICTURE_TEST_ID = 'picture-icon'
    const picture = getByTestId(PICTURE_TEST_ID)
    expect(picture).toHaveAttribute('src', expect.stringContaining(TEST_SRC))
    expect(picture).toHaveAttribute('alt', expect.stringContaining(TEST_ALT))
  })
})

// Put yourself in the mind of the user when writing tests, and only tests things that interest them

/* Other tests could be:
1. it renders correctly, for components that have rendered output influenced by props
2. it handles state correctly, for components with state and conditionals
3. it handles events correctly, for components with interaction and events
4. it handles edge cases, for components using arrays or user data such as age
*/
