import React from 'react'
import {
  Container,
  Row,
  Col
} from 'react-bootstrap'

import Picture from 'components/Picture'
import SkillTag from 'components/SkillTag'
import Thumbnail from 'components/Thumbnail'
import {
  CONTENTBLOCK_TYPE_CIRCLE,
  CONTENTBLOCK_TYPE_SQUARE
} from 'constants.js'
import 'styles/styles.scss'
import loadedInfo from 'content/info.json'

const structure = [
  {
    id: 'about-me',
    headerText: 'About Me',
    blocks: [
      {
        type: CONTENTBLOCK_TYPE_SQUARE,
        content: (
          <>
            <div className='spacer-md-400' />
            <ul>
              {loadedInfo.summaryPoints.map((info, i) => {
                const className =
                  i === 0
                    ? 'list-item-lg'
                    : i === 1
                      ? 'list-item-md'
                      : 'list-item-sm'
                return (
                  <li
                    key={`summary-point-${i}`}
                    className={className}
                  >
                    {info}
                  </li>
                )
              })}
            </ul>
            <div className='spacer-md-400' />
          </>
        )
      },
      {
        type: CONTENTBLOCK_TYPE_CIRCLE,
        content: (
          <Picture
            src={process.env.PUBLIC_URL + '/img/me.jpg'}
            alt='Profile Picture'
            className='picture-profile'
          />
        )
      },
      {
        type: CONTENTBLOCK_TYPE_SQUARE,
        content: (
          <>
            <div className='container-trimmed container-full d-none d-lg-block'>
              <ul className='list-professional-achievements'>
                {loadedInfo.topProfessionalAchievements.map((info, i) => {
                  return (
                    <li
                      key={`professional-achievement-${i}`}
                      className='list-item-professional-achievements'
                    >
                      {info}
                    </li>
                  )
                })}
              </ul>
            </div>
            <div className='container-trimmed container-full d-lg-none'>
              <div className='spacer-sm-400' />
              <p className='text-location'>
                <Picture
                  src={process.env.PUBLIC_URL + '/img/icon-location.png'}
                  alt='Currently Located'
                  className='picture-icon-sm'
                />
                <span>{loadedInfo.currentLocation}</span>
              </p>
              <p className='text-based'>
                <Picture
                  src={process.env.PUBLIC_URL + '/img/icon-based.png'}
                  alt='Based In'
                  className='picture-icon-sm'
                />
                <span>{loadedInfo.currentlyBased}</span>
              </p>
              <div className='spacer-sm-400' />
            </div>
          </>
        )
      },
      {
        type: CONTENTBLOCK_TYPE_SQUARE,
        content: (
          <>
            <div className='container-trimmed container-full d-none d-lg-block'>
              <div className='spacer-sm-400' />
              <p className='text-location'>
                <Picture
                  src={process.env.PUBLIC_URL + 'img/icon-location.png'}
                  alt='Currently Located'
                  className='picture-icon-sm'
                />
                <span>{loadedInfo.currentLocation}</span>
              </p>
              <p className='text-based'>
                <Picture
                  src={process.env.PUBLIC_URL + '/img/icon-based.png'}
                  alt='Based In'
                  className='picture-icon-sm'
                />
                <span>{loadedInfo.currentlyBased}</span>
              </p>
              <div className='spacer-sm-400' />
            </div>
            <div className='container-trimmed container-full d-lg-none'>
              <ul className='list-professional-achievements'>
                {loadedInfo.topProfessionalAchievements.map((info, i) => {
                  return (
                    <li
                      key={`professional-achievement-${i}`}
                      className='list-item-professional-achievements'
                    >
                      {info}
                    </li>
                  )
                })}
              </ul>
            </div>
          </>
        )
      }
    ]
  },
  {
    id: 'testimonials',
    headerText: 'Testimonials',
    blocks: loadedInfo.testimonials.map((info) => {
      return {
        type: CONTENTBLOCK_TYPE_SQUARE,
        content: (
          <div className='testimonial-container'>
            <div className='spacer-md-400' />
            <span className='testimonial-quotation-mark'>
              "
            </span>
            <span className='testimonial-text'>
              {info.text}
            </span>
            <span className='testimonial-quotation-mark'>
              "
            </span>
            <span className='testimonial-spacer'>
              &ensp;-&ensp;
            </span>
            <span className='testimonial-by'>
              {info.by}
            </span>
            <span className='testimonial-at-symbol'>
              &ensp;@&ensp;
            </span>
            <span className='testimonial-at'>
              {info.at}
            </span>
            <div className='spacer-md-400' />
          </div>
        )
      }
    })
  },
  {
    id: 'skills',
    headerText: 'Skills',
    blocks: [
      {
        type: CONTENTBLOCK_TYPE_SQUARE,
        content: (
          <Container className='container-skill-tags container-trimmed container-full'>
            <Row className='container-trimmed container-full'>
              <p>
                Key: <span className='red'>Frontend</span>
                , <span className='blue'>Backend</span>
                , <span className='green'>DevOps</span>
                , <span className='pink'>Games</span>
                , <span className='orange'>Meta</span>
              </p>
              {loadedInfo.skillTags.map((info, i) => {
                return (
                  <SkillTag
                    key={`skill-tag-${i}`}
                    type={info.type}
                    text={info.text}
                  />
                )
              })}
            </Row>
          </Container>
        )
      },
      {
        type: CONTENTBLOCK_TYPE_SQUARE,
        content: (
          <ul className='list-top-attributes'>
            {loadedInfo.topAttributes.map((info, i) => {
              return (
                <li
                  key={`top-attribute-${i}`}
                  className='list-item-top-attributes'
                >
                  {info}
                </li>
              )
            })}
          </ul>
        )
      }
    ]
  },
  {
    id: 'experience',
    headerText: 'Experience',
    blocks: loadedInfo.experience.map((info, i) => {
      return {
        type: CONTENTBLOCK_TYPE_SQUARE,
        content: (
          <Container className='container-experience container-trimmed container-full'>
            <Row className='container-trimmed container-full'>
              <h2 className='text-experience-header'>{info.endDate} - {info.startDate}</h2>
              <h3 className='text-experience-subheader'>{info.company}</h3>
              <div className='spacer-sm-400' />
              <h4 className='text-experience-tagline'>{info.role}</h4>
              <div className='spacer-sm-400' />
              <p className='text-experience-links'>
                {info.links.map((link, j) => {
                  return (
                    <a
                      key={`experience-link-${j}`}
                      href={link.url}
                      target='blank'
                      rel='noopener noreferer'
                      className='link-experience'
                    >
                      {link.text}
                    </a>
                  )
                })}
              </p>
              <div className='spacer-sm-400' />
              {info.skillTagIds.map((id, k) => {
                const skill = loadedInfo.skillTags[id]
                return (
                  <SkillTag
                    key={`experience-skill-tag-${k}`}
                    type={skill.type}
                    text={skill.text}
                  />
                )
              })}
              <div className='spacer-sm-400' />
              <p className='text-experience-description'>
                {info.description}
              </p>
              <div className='spacer-sm-400' />
              {info.screenshotPaths.map((path, l) => {
                return (
                  <Thumbnail
                    key={`${info.company}-screenshot-${l}`}
                    src={path}
                    alt={`${info.company} Screenshot ${l + 1}`}
                  />
                )
              })}
            </Row>
          </Container>
        )
      }
    })
  },
  {
    id: 'code-samples',
    headerText: 'Code Samples',
    blocks: loadedInfo.codeSamples.map((info, i) => {
      return {
        type: CONTENTBLOCK_TYPE_SQUARE,
        content: (
          <Container className='container-code-samples container-trimmed container-full'>
            <Row className='container-trimmed container-full'>
              <h4 className='text-code-samples-header'>{info.title}</h4>
              <div className='spacer-sm-400' />
              <p className='text-code-samples-description'>{info.description}</p>
              <div className='spacer-sm-400' />
              {info.skillTagIds.map((id, j) => {
                const skill = loadedInfo.skillTags[id]
                return (
                  <SkillTag
                    key={`code-sample-skill-tag-${j}`}
                    type={skill.type}
                    text={skill.text}
                  />
                )
              })}
              {info.url && (
                <>
                  <div className='spacer-sm-400' />
                  <Container className='container-trimmed container-full'>
                    <Row className='container-trimmed container-full'>
                      <Col className='container-trimmed container-full' />
                      <Col
                        xs='auto'
                        className='container-trimmed container-full'
                      >
                        <a
                          key={`code-sample-link-${i}`}
                          href={info.url}
                          target='blank'
                          rel='noopener noreferer'
                          className='link-code-samples'
                        >
                          <Picture
                            src={process.env.PUBLIC_URL + '/img/icon-save.png'}
                            alt='Download Code Sample'
                            className='picture-icon-md'
                          />
                          <p className='text-code-samples-url'>Download Me</p>
                        </a>
                      </Col>
                      <Col className='container-trimmed container-full' />
                    </Row>
                  </Container>
                </>
              )}
            </Row>
          </Container>
        )
      }
    })
  },
  {
    id: 'background',
    headerText: 'Background',
    blocks: [
      {
        type: CONTENTBLOCK_TYPE_SQUARE,
        content: (
          <Container className='container-background container-trimmed container-full'>
            <Row className='container-trimmed container-full'>
              <h4 className='text-background-education-header'>{loadedInfo.education.course}</h4>
              <p className='text-background-education-sub-header'>{loadedInfo.education.school}</p>
              <div className='spacer-sm-400' />
              <p className='text-background-education-body'>Of possible modules I pioritised mostly programming ones, with some exposure to art & design also.</p>
              <div className='spacer-sm-400' />
              <p className='text-background-education-body'>Graduated in {loadedInfo.education.graduatedIn} with grade {loadedInfo.education.grade}</p>
            </Row>
          </Container>
        )
      },
      {
        type: CONTENTBLOCK_TYPE_SQUARE,
        content: (
          <Container className='container-background container-trimmed container-full'>
            <Row className='container-trimmed container-full'>
              <ul className='list-background-top-personal-achievements'>
                {loadedInfo.topPersonalAchievements.map((info, m) => {
                  return (
                    <li
                      key={`personal-achievement-${m}`}
                      className='list-item-background-top-personal-achievements'
                    >
                      {info}
                    </li>
                  )
                })}
              </ul>
            </Row>
          </Container>
        )
      },
      {
        type: CONTENTBLOCK_TYPE_CIRCLE,
        content: (
          <Picture
            src={process.env.PUBLIC_URL + '/img/showing-off.jpg'}
            alt='More Sunset Times'
            className='picture-showing-off'
          />
        )
      },
      {
        type: CONTENTBLOCK_TYPE_SQUARE,
        content: (
          <Container className='container-background container-trimmed container-full'>
            <Row className='container-trimmed container-full'>
              <ul className='list-background-hobbies'>
                {loadedInfo.hobbies.map((info, n) => {
                  return (
                    <li
                      key={`hobby-${n}`}
                      className='list-item-background-hobbies'
                    >
                      {info}
                    </li>
                  )
                })}
              </ul>
            </Row>
          </Container>
        )
      }
    ]
  }
]

export default structure
