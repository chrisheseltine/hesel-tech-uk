export const isFalsy = (item) => {
  if (typeof item === 'number') {
    if (item === null || item === undefined || item === 0 || isNaN(item)) return true
    else return false
  } else {
    if (item === null || item === undefined) return true
    else return false
  }
}
