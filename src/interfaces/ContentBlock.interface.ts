import { ReactElement } from 'react'

export default interface IContentBlock {
  type: string
  content: ReactElement
}
