import React, { ReactElement } from 'react'

import NavBar from 'components/NavBar'
import PictureHeader from 'components/PictureHeader'
import TitleBar from 'components/TitleBar'
import ContentExplorer from 'components/ContentExplorer'
import Footer from 'components/Footer'

import 'styles/styles.scss'

const App: React.FC<Record<string, never>> = (): ReactElement => {
  return (
    <div className='container-trimmed'>
      <NavBar />
      <div id='top' />
      <PictureHeader />
      <TitleBar />
      <ContentExplorer />
      <Footer />
    </div>
  )
}

export default App
